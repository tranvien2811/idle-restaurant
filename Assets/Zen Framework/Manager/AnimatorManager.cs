﻿using UnityEngine;

public class AnimatorManager : MonoBehaviour
{
    private static bool IsContain(Animator animator, string anim)
    {
        bool isResult = false;
        if (animator != null)
        {
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                AnimationClip clip = animator.runtimeAnimatorController.animationClips[i];
                if (clip.name.Equals(anim))
                    isResult = true;
            }
        }

        return isResult;
    }

    public static void Enable(Animator animator, bool isStatus)
    {
        if (animator != null)
            animator.enabled = isStatus;
    }

    public static void Play(Animator animator, string anim)
    {
        if (animator == null || !IsContain(animator, anim))
            return;
        animator.Play(anim);
    }

    public static void SetRuntimeController(Animator output, Animator input)
    {
        if (output == null || input == null)
            return;
        output.runtimeAnimatorController = input.runtimeAnimatorController;
    }

    public static void SetBool(Animator animator, string key, bool isStatus)
    {
        if (animator != null)
            animator.SetBool(key, isStatus);
    }

    public static void SetFloat(Animator animator, string key, float value)
    {
        if (animator != null)
            animator.SetFloat(key, value);
    }

    public static void SetInt(Animator animator, string key, float value)
    {
        if (animator != null)
            animator.SetFloat(key, value);
    }

    public static void SetFrameRate(Animator animator, string anim, float rate)
    {
        if (animator == null || !IsContain(animator, anim))
            return;
        for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
        {
            AnimationClip clip = animator.runtimeAnimatorController.animationClips[i];
            if (clip.name.Equals(anim))
                clip.frameRate = rate;
        }
    }

    public static AnimationClip GetClip(Animator animator, string anim)
    {
        AnimationClip clip = null;
        if (animator != null && IsContain(animator, anim))
        {
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                if (animator.runtimeAnimatorController.animationClips[i].name.Equals(anim))
                    clip = animator.runtimeAnimatorController.animationClips[i];
            }
        }

        return clip;
    }

    public static float GetLenght(Animator animator, string anim)
    {
        float lenght = 0;
        if (animator != null && IsContain(animator, anim))
        {
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                AnimationClip clip = animator.runtimeAnimatorController.animationClips[i];
                if (clip.name.Equals(anim))
                    lenght = clip.length;
            }
        }

        return lenght;
    }

    public static float GetSpeed(Animator animator, string anim)
    {
        float speed = 0;
        if (animator != null && IsContain(animator, anim))
        {
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                AnimationClip clip = animator.runtimeAnimatorController.animationClips[i];
                if (clip.name.Equals(anim))
                    speed = clip.apparentSpeed;
            }
        }

        return speed;
    }

    public static int GetAnimationIndex(Animator animator, string anim)
    {
        int number = 0;
        if (animator != null && IsContain(animator, anim))
        {
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                AnimationClip clip = animator.runtimeAnimatorController.animationClips[i];
                if (clip.name.Equals(anim))
                    number = i;
            }
        }

        return number;
    }

    public static int GetAnimation(Animator animator)
    {
        int number = 0;
        if (animator != null)
            number = animator.runtimeAnimatorController.animationClips.Length;
        return number;
    }
}