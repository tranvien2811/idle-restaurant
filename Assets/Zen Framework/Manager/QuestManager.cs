using UnityEngine;
using System.Collections.Generic;
using System.IO;

[System.Serializable]
public enum Quest
{
    None
}

[System.Serializable]
public class QuestDataBase
{
    public Quest quest;
    public int collected;
    public int pupose;
    public int reward;
}

[System.Serializable]
public class QuestData
{
    public List<QuestDataBase> dataBases = new List<QuestDataBase>();
}

[System.Serializable]
public struct QuestReward
{
    public string reward;
    public int firstMile;
    public int mileRaise;
}

[System.Serializable]
public struct QuestBase
{
    public Quest quest;
    public Sprite icon;
    public string note;
    public int firstMile;
    public int mileRaise;

    public QuestReward rewards;
}

[System.Serializable]
public struct QuestRewardEvent
{
    public Quest quest;
    public int value;
}

public class QuestManager : MonoBehaviour
{
    private QuestData questData;

    public List<QuestBase> bases;

    private void OnEnable()
    {
        Init();
    }

    private void Init()
    {
        if (bases.Count <= 0)
            return;
        questData = new QuestData();
        for (int i = 0; i < bases.Count; i++)
        {
            QuestDataBase save = new QuestDataBase()
            {
                quest = bases[i].quest,
                collected = 0,
                pupose = bases[i].firstMile,
                reward = bases[i].rewards.firstMile
            };
            questData.dataBases.Add(save);
        }

        Load();
    }

    private void Load()
    {
        if (!File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "quest_data.xml"))
            return;
#if UNITY_EDITOR
        questData = XmlPath.GetByResources<QuestData>("quest_data");
#else
        questData = XmlPath.GetByPlatform<QuestData>("quest_data.xml");
#endif
    }

    private void Save()
    {
//        XmlPath.SetByPlatform("quest_data", questData);
        Eventon<QuestData>.Trigger(questData);
    }

    public void Collect(Quest quest, int value)
    {
        for (int i = 0; i < questData.dataBases.Count; i++)
        {
            if (!questData.dataBases[i].quest.Equals(quest))
                return;
            questData.dataBases[i].collected += value;
            Save();
        }
    }

    public void Reward(Quest quest)
    {
        for (int i = 0; i < questData.dataBases.Count; i++)
        {
            if (!questData.dataBases[i].quest.Equals(quest) ||
                questData.dataBases[i].collected < questData.dataBases[i].pupose)
                continue;
            Eventon<QuestRewardEvent>.Trigger(new QuestRewardEvent()
            {
                quest = quest,
                value = questData.dataBases[i].reward
            });
            questData.dataBases[i].pupose += bases[i].mileRaise;
            questData.dataBases[i].reward += bases[i].rewards.mileRaise;
            Save();
        }
    }

    public List<Quest> GetDones()
    {
        List<Quest> dones = new List<Quest>();
        for (int i = 0; i < bases.Count; i++)
        {
            if (IsDone(bases[i].quest))
                dones.Add(bases[i].quest);
        }

        return dones;
    }

    public Sprite GetIcon(Quest quest)
    {
        Sprite icon = null;
        for (int i = 0; i < bases.Count; i++)
        {
            if (bases[i].quest.Equals(quest))
                icon = bases[i].icon;
        }

        return icon;
    }

    public string GetNote(Quest quest)
    {
        string note = null;
        for (int i = 0; i < bases.Count; i++)
        {
            if (bases[i].quest.Equals(quest))
                note = bases[i].note;
        }

        return note;
    }

    public bool IsDone(Quest quest)
    {
        bool isResult = false;
        for (int i = 0; i < questData.dataBases.Count; i++)
        {
            if (!questData.dataBases[i].quest.Equals(quest))
                continue;
            int collected = questData.dataBases[i].collected;
            int pupose = questData.dataBases[i].pupose;
            if (collected < pupose)
                continue;
            isResult = true;
        }

        return isResult;
    }
}