﻿using System;
using UnityEngine;

[System.Serializable]
public class TimeOfDay
{
    public int hour;
    public int minute;
    public int second;
}

[System.Serializable]
public class TimeOfYear
{
    public int day;
    public int month;
    public int year;
}

[System.Serializable]
public class TimeData
{
    public TimeOfDay timeOfDay = new TimeOfDay();
    public TimeOfYear timeOfYear = new TimeOfYear();
}

public class TimeManager : MonoBehaviour
{
    public static void Scale(float value)
    {
        Time.timeScale = value;
    }

    public static TimeOfDay GetDayRange(DateTime time)
    {
        TimeOfDay range = new TimeOfDay();
        range.hour = 23 - time.Hour;
        range.minute = 59 - time.Minute;
        range.second = 59 - time.Second;

        return range;
    }

    public static TimeData GetTimeRange(DateTime time)
    {
        TimeData range = new TimeData();
        range.timeOfYear.year = time.Year - DateTime.Now.Year;
        range.timeOfYear.month = time.Month - DateTime.Now.Month;
        range.timeOfYear.day = time.Day - DateTime.Now.Day;

        range.timeOfDay.hour = time.Hour - DateTime.Now.Hour;
        range.timeOfDay.minute = time.Minute - DateTime.Now.Minute;
        range.timeOfDay.second = time.Second - DateTime.Now.Second;

        return range;
    }
}