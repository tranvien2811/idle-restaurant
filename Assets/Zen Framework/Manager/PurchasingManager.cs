using UnityEngine;
using UnityEngine.Purchasing;

[System.Serializable]
public enum PurchasingState
{
    None,
    Successed,
    Failed
}

[System.Serializable]
public class Products
{
    public string id;
    public int value;
}

public class PurchasingManager : MonoBehaviour, IStoreListener
{
    private static PurchasingManager mInstance;

    private IStoreController controller;
    private IExtensionProvider extensions;
    private bool isInit;

    public Products[] products;

    #region property

    public static PurchasingManager Instance
    {
        get
        {
            if (mInstance == null)
                mInstance = FindObjectOfType<PurchasingManager>();
            return mInstance;
        }
    }

    #endregion

    private void OnEnable()
    {
        Init();
    }

    private void Init()
    {
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        for (int i = 0; i < products.Length; i++)
        {
            builder.AddProduct(products[i].id,
                ProductType.Consumable, new IDs()
                {
                    {products[i].id, GooglePlay.Name},
                    {products[i].id, AppleAppStore.Name}
                });
        }

        UnityPurchasing.Initialize(this, builder);
    }

    private void OnReward(string id)
    {
        for (int i = 0; i < products.Length; i++)
        {
            if (!products[i].id.Equals(id))
                continue;
            Eventon<PurchasingState>.Trigger(PurchasingState.Successed);
        }
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
#if UNITY_EDITOR
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
#endif
        isInit = false;
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
#if UNITY_EDITOR
        Debug.Log("OnPurchaseReward: Success. Product:" + e.purchasedProduct);
#endif
        OnReward(e.purchasedProduct.definition.id);
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
    {
#if UNITY_EDITOR
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",
            i.definition.storeSpecificId, p));
#endif
        Eventon<PurchasingState>.Trigger(PurchasingState.Failed);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
#if UNITY_EDITOR
        Debug.Log("OnInitialized: pass");
#endif
        this.controller = controller;
        this.extensions = extensions;
        isInit = true;
    }

    public bool IsInit()
    {
        return isInit;
    }

    public int GetLenght()
    {
        int count = 0;
        if (products.Length > 0)
            count = products.Length;
        return count;
    }

    public string GetLocalizedPrice(int index)
    {
        string priceString = "In Coming";
        if (products.Length > 0 &&
            products[index].id != null)
        {
            Product product = controller.products.WithID(products[index].id);
            if (product != null)
                priceString = product.metadata.localizedPriceString;
        }

        return priceString;
    }

    public void Buy(int index)
    {
        if (index >= products.Length || index <= -1 || products[index].id == null)
            return;
        controller.InitiatePurchase(products[index].id);
    }
}