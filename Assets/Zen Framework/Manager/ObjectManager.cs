﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : MonoBehaviour
{
    private Type type;
    private Transform pool;
    private Vector3 scale;
    private string name;

    public void Init(Type type, Transform pool, Vector3 scale, string name)
    {
        this.type = type;
        this.pool = pool;
        this.scale = scale;
        this.name = name;
    }

    public void Return()
    {
        transform.localScale = scale;
        if (pool != null)
        {
            transform.position = pool.transform.position;
            transform.SetParent(pool.transform, true);
        }

        gameObject.SetActive(false);
    }

    public Type GetType()
    {
        return type;
    }

    public string GetName()
    {
        return name;
    }
}

public class ObjectManager : MonoBehaviour
{
    private static List<GameObject> objs = new List<GameObject>();

    public static GameObject Init(Type type, Transform pool, Vector3 local, string path, bool isActive = false)
    {
        GameObject prefab = Resources.Load<GameObject>(path);
        GameObject clone = null;

        if (prefab != null)
        {
            clone = Instantiate(prefab, pool);
            clone.transform.position = local;
            clone.SetActive(isActive);
            ObjectPooling obj = clone.AddComponent<ObjectPooling>();
            obj.Init(type, pool, prefab.transform.localScale, prefab.name);
            if (!objs.Contains(obj.gameObject))
                objs.Add(obj.gameObject);
        }

        return clone;
    }

    public static GameObject Call(Type type, Transform pool, Vector3 local, string path, bool isActive = true)
    {
        List<GameObject> readys = new List<GameObject>();
        GameObject prefab = Resources.Load<GameObject>(path);
        GameObject clone = null;

        if (prefab != null)
        {
            for (int i = 0; i < objs.Count; i++)
            {
                if (objs[i] != null)
                {
                    if (objs[i].activeInHierarchy || !objs[i].GetComponent<ObjectPooling>())
                        continue;
                    ObjectPooling objPooling = objs[i].GetComponent<ObjectPooling>();
                    if (objPooling.GetType() != type || !objPooling.GetName().Equals(prefab.name))
                        continue;
                    readys.Add(objs[i]);
                }
                else
                    objs.Remove(objs[i]);
            }

            if (readys.Count <= 0)
            {
                GameObject additional = Instantiate(prefab, pool);
                ObjectPooling objPooling = additional.AddComponent<ObjectPooling>();
                objPooling.Init(type, pool, prefab.transform.localScale, prefab.name);
                objs.Add(additional);
                readys.Add(additional);
            }

            clone = readys[0].gameObject;
            clone.transform.position = local;
            clone.SetActive(isActive);
        }

        return clone;
    }

    public static GameObject[] CallAll(Type type, Transform pool, Vector3 local, string path, bool isActive = true)
    {
        List<GameObject> readys = new List<GameObject>();
        GameObject[] prefabs = Resources.LoadAll<GameObject>(path);
        GameObject[] clones = new GameObject[0];

        if (prefabs != null || prefabs.Length > 0)
        {
            for (int i = 0; i < objs.Count; i++)
            {
                if (objs[i] != null)
                {
                    if (objs[i].activeInHierarchy || !objs[i].GetComponent<ObjectPooling>())
                        continue;
                    ObjectPooling objPooling = objs[i].GetComponent<ObjectPooling>();
                    if (objPooling.GetType() != type)
                        continue;
                    for (int j = 0; j < prefabs.Length; j++)
                    {
                        if (objPooling.GetName().Equals(prefabs[j].name))
                            readys.Add(objs[i]);
                    }
                }
                else
                    objs.Remove(objs[i]);
            }

            int range = prefabs.Length - readys.Count;
            for (int i = 0; i < range; i++)
            {
                GameObject additional = Instantiate(prefabs[i], pool);
                ObjectPooling objPooling = additional.AddComponent<ObjectPooling>();
                objPooling.Init(type, pool, prefabs[i].transform.localScale, prefabs[i].name);
                objs.Add(additional);
                readys.Add(additional);
            }

            clones = new GameObject[readys.Count];
            for (int i = 0; i < readys.Count; i++)
            {
                clones[i] = readys[i].gameObject;
                readys[i].transform.position = local;
                readys[i].gameObject.SetActive(isActive);
            }
        }

        return clones;
    }
}