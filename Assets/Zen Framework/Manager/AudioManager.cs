﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[System.Serializable]
public enum Audio
{
    All,
    Sound,
    Music
}

[System.Serializable]
public class AudioData
{
    public bool isSound;
    public bool isMusic;
}

[System.Serializable]
public struct LoopClip
{
    public Audio Loop;
    public AudioClip loopClip;
    public Coroutine loopCoroutine;
}

public class AudioManager : MonoBehaviour
{
    private List<LoopClip> loopClips = new List<LoopClip>();
    private AudioData audioData;

    public AudioSource sound;
    public AudioSource music;

    private void OnEnable()
    {
        Init();
    }

    private bool IsNullLoopClip(Audio audio, AudioClip clip)
    {
        bool isResult = false;
        for (int i = 0; i < loopClips.Count; i++)
        {
            if (!loopClips[i].Loop.Equals(audio) || !loopClips[i].loopClip.Equals(clip))
                continue;
            isResult = true;
        }

        return isResult;
    }

    private void Load()
    {
        if (audioData == null)
            return;
        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "audio_data.xml"))
            audioData = XmlPath.GetByPlatform<AudioData>("audio_data");
        if (music != null)
            music.mute = !audioData.isMusic;
        if (sound != null)
            sound.mute = !audioData.isSound;
    }

    private void Init()
    {
        audioData = new AudioData()
        {
            isSound = true,
            isMusic = true
        };
        if (music != null)
            music.mute = false;
        if (sound != null)
            sound.mute = false;
        Load();
    }

    private void Save()
    {
//        XmlPath.SetByPlatform("audio_data", audioData);
        Eventon<AudioData>.Trigger(audioData);
    }

    private void StopAllLoopClip(Audio audio)
    {
        for (int i = 0; i < loopClips.Count; i++)
        {
            if (!loopClips[i].Loop.Equals(audio) || loopClips[i].loopCoroutine == null)
                continue;
            StopCoroutine(loopClips[i].loopCoroutine);
            loopClips.Remove(loopClips[i]);
        }
    }

    private IEnumerator OnLoopPlay(Audio audio, AudioClip clip, float time)
    {
        yield return new WaitForSecondsRealtime(time);
        if (clip != null)
        {
            switch (audio)
            {
                case Audio.All:
                    if (sound != null)
                        sound.PlayOneShot(clip);
                    if (music != null)
                        music.PlayOneShot(clip);
                    break;

                case Audio.Sound:
                    if (sound != null)
                        sound.PlayOneShot(clip);
                    break;

                case Audio.Music:
                    if (music != null)
                        music.PlayOneShot(clip);
                    break;
            }

            AddLoopClip(audio, clip, time);
        }
    }

    private void AddLoopClip(Audio audio, AudioClip clip, float time)
    {
        if (IsNullLoopClip(audio, clip))
        {
            LoopClip loopBase = new LoopClip()
            {
                Loop = audio,
                loopClip = clip,
                loopCoroutine = StartCoroutine(OnLoopPlay(audio, clip, time))
            };
            loopClips.Add(loopBase);
        }
        else
        {
            for (int i = 0; i < loopClips.Count; i++)
            {
                if (!loopClips[i].Loop.Equals(audio) || !loopClips[i].loopClip.Equals(clip))
                    continue;
                var loopAudioClip = loopClips[i];
                loopAudioClip.loopCoroutine = StartCoroutine(OnLoopPlay(audio, clip, time));
                loopClips[i] = loopAudioClip;
            }
        }
    }

    public void Play(Audio audio, AudioClip clip, bool isLoop)
    {
        if (clip == null)
            return;
        switch (audio)
        {
            case Audio.All:
                if (sound != null)
                    sound.PlayOneShot(clip);
                if (music != null)
                    music.PlayOneShot(clip);
                break;

            case Audio.Sound:
                if (sound != null)
                    sound.PlayOneShot(clip);
                break;

            case Audio.Music:
                if (music != null)
                    music.PlayOneShot(clip);
                break;
        }

        if (isLoop)
            AddLoopClip(audio, clip, clip.length);
    }

    public void StopLoopClip(AudioClip clip)
    {
        for (int i = 0; i < loopClips.Count; i++)
        {
            if (!loopClips[i].loopClip.Equals(clip) || loopClips[i].loopCoroutine == null)
                continue;
            StopCoroutine(loopClips[i].loopCoroutine);
            loopClips.Remove(loopClips[i]);
        }
    }

    public void Stop(Audio audio)
    {
        switch (audio)
        {
            case Audio.All:
                if (sound != null)
                    sound.Stop();
                if (music != null)
                    music.Stop();
                StopAllLoopClip(Audio.Sound);
                StopAllLoopClip(Audio.Music);
                break;

            case Audio.Sound:
                if (sound != null)
                    sound.Stop();
                StopAllLoopClip(Audio.Sound);
                break;

            case Audio.Music:
                if (music != null)
                    music.Stop();
                StopAllLoopClip(Audio.Music);
                break;
        }
    }

    public void Inverse(Audio audio)
    {
        switch (audio)
        {
            case Audio.All:
                audioData.isSound = !audioData.isSound;
                audioData.isMusic = !audioData.isMusic;
                break;

            case Audio.Sound:
                audioData.isSound = !audioData.isSound;
                break;

            case Audio.Music:
                audioData.isMusic = !audioData.isMusic;
                break;
        }

        if (music != null)
            music.mute = !audioData.isMusic;
        if (sound != null)
            sound.mute = !audioData.isSound;

        Save();
    }

    public AudioData GetData()
    {
        AudioData cloneData = new AudioData()
        {
            isSound = true,
            isMusic = true
        };
        if (audioData != null)
            cloneData = audioData;
        return cloneData;
    }
}