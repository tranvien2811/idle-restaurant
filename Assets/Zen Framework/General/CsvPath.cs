﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;

public static class CsvPath
{
    private static char[] separators = new char[]
    {
        ',',
        '\n'
    };

    public static void SetByPath(string path, List<string> values, int row, int column)
    {
        StreamWriter writer = new StreamWriter(Application.dataPath + path + ".csv");
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < column; j++)
            {
                if (j < column - 1)
                    writer.Write(values[j] + ',');
                else
                    writer.Write(values[j]);
            }

            writer.Write('\n');
        }

        writer.Close();
    }

    public static void SetByPlatform(string file, List<string> values, int row, int column)
    {
        StreamWriter writer = new StreamWriter(
            PlatformPath.GetPath(PlatformPath.GetPlatform()) + file + ".csv");
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < column; j++)
            {
                if (j < column - 1)
                    writer.Write(values[j] + ',');
                else
                    writer.Write(values[j]);
            }

            writer.Write('\n');
        }

        writer.Close();
    }

    public static List<string> GetByPath(string path)
    {
        List<string> strings = new List<string>();

        StreamReader reader = new StreamReader(Application.dataPath + path + ".csv");
        if (File.Exists(Application.dataPath + path + ".csv"))
        {
            while (!reader.EndOfStream)
            {
                string[] lines = reader.ReadLine().Split(separators);
                if (lines != null)
                    for (int i = 0; i < lines.Length; i++)
                    {
                        strings.Add(lines[i]);
                    }
            }
        }

        reader.Close();
        return strings;
    }

    public static List<string> GetByPlatform(string file)
    {
        List<string> strings = new List<string>();

        StreamReader reader = new StreamReader(PlatformPath.GetPlatform() + file + ".csv");
        if (File.Exists(PlatformPath.GetPlatform() + file + ".csv"))
        {
            while (!reader.EndOfStream)
            {
                string[] lines = reader.ReadLine().Split(separators);
                if (lines != null)
                    for (int i = 0; i < lines.Length; i++)
                    {
                        strings.Add(lines[i]);
                    }
            }
        }

        reader.Close();
        return strings;
    }
}