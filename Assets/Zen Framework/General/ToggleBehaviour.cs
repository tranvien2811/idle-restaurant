using UnityEngine;

public abstract class ToggleBehaviour : MonoBehaviour
{
    protected abstract void OnAppear();
    protected abstract void OnCyclic();
    protected abstract void OnDisappear();

    private void OnEnable()
    {
        OnAppear();
    }

    private void Update()
    {
        OnCyclic();
    }

    private void OnDisable()
    {
        OnDisappear();
    }
}