public static class Eventon<T>
{
    public delegate void Delegate<T>(T param);

    public static event Delegate<T> Static;

    public static event Delegate<T> Dynamic
    {
        add
        {
            Static += value;
            Static += param => { Static -= value; };
        }
        remove => Static -= value;
    }

    public static void Trigger(T param)
    {
        if (Static != null && param != null)
            Static(param);
    }
}