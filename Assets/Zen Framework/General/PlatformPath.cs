﻿using UnityEngine;

[System.Serializable]
public enum Platform
{
    Resources,
    Android,
    Ios,
    Else
}

public static class PlatformPath
{
    public static Platform GetPlatform()
    {
        Platform platform = Platform.Resources;
#if UNITY_EDITOR
        platform = Platform.Resources;
#elif UNITY_ANDROID
        platform = Platform.Android;
#elif UNITY_IOS
        platform = Platform.Ios;
#else
        platform = Platform.Else;
#endif
        return platform;
    }

    public static string GetPath(Platform platform)
    {
        string path = null;
        switch (platform)
        {
            case Platform.Resources:
                path = Application.dataPath + "/Resources/";
                break;
            case Platform.Android:
                path = Application.persistentDataPath;
                break;
            case Platform.Ios:
                path = Application.persistentDataPath + "/";
                break;
            case Platform.Else:
                path = Application.dataPath + "/";
                break;
        }

        return path;
    }
}