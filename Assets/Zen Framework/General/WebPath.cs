﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class WebPath : MonoBehaviour
{
    private byte[] datas;

    public IEnumerator OnGetdata(string uri)
    {
        datas = null;
        using (UnityWebRequest request = UnityWebRequest.Get(uri))
        {
            yield return request.SendWebRequest();
            if (request.isError || request.isHttpError)
            {
#if UNITY_EDITOR
                Debug.Log(request.error);
#endif
            }
            else
            {
                if (request.isDone)
                    datas = request.downloadHandler.data;
            }
        }
    }

    public IEnumerator OnPostdata(string uri, WWWForm form)
    {
        datas = null;
        using (UnityWebRequest request = UnityWebRequest.Post(uri, form))
        {
            yield return request.SendWebRequest();
            if (request.isError || request.isHttpError)
            {
#if UNITY_EDITOR
                Debug.Log(request.error);
#endif
            }
            else
                datas = request.downloadHandler.data;
        }
    }

    public byte[] GetDownloadedDatas()
    {
        return datas;
    }
}