﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum Parabol
{
    Right,
    Left,
    Up,
    Down,
    Forward,
    Back
}

[System.Serializable]
public class ParabolaBase
{
    public Parabol kind;
    public Transform transformer;
    public Vector3 startPosition;
    public Vector3 endPosition;
    public float time;
    public float speed;
}

public class CurveParabola : MonoBehaviour
{
    private List<ParabolaBase> parabolaBases = new List<ParabolaBase>();

    [SerializeField] private AnimationCurve animationCurve;

    private void Update()
    {
        Translating();
    }

    private void Translating()
    {
        if (parabolaBases.Count <= 0)
            return;
        for (int i = 0; i < parabolaBases.Count; i++)
        {
            if (parabolaBases[i].transformer != null)
            {
                if (parabolaBases[i].transformer.position != parabolaBases[i].endPosition)
                {
                    parabolaBases[i].time += (parabolaBases[i].speed * Time.deltaTime);
                    Vector3 lerp = Vector3.Lerp(parabolaBases[i].startPosition, parabolaBases[i].endPosition,
                        parabolaBases[i].time);
                    switch (parabolaBases[i].kind)
                    {
                        case Parabol.Right:
                            lerp.x += animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                        case Parabol.Left:
                            lerp.x -= animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                        case Parabol.Up:
                            lerp.y += animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                        case Parabol.Down:
                            lerp.y -= animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                        case Parabol.Forward:
                            lerp.z += animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                        case Parabol.Back:
                            lerp.z -= animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                    }

                    parabolaBases[i].transformer.position = lerp;
                }
                else
                    parabolaBases.Remove(parabolaBases[i]);
            }
            else
                parabolaBases.Remove(parabolaBases[i]);
        }
    }

    public void AddBase(Parabol kind, Transform transformer, Vector3 endPosition, float speed)
    {
        if (transformer == null)
            return;
        ParabolaBase newBase = new ParabolaBase()
        {
            kind = kind,
            transformer = transformer,
            startPosition = transformer.position,
            endPosition = endPosition,
            time = 0,
            speed = speed
        };

        parabolaBases.Add(newBase);
    }

    public void RemoveBase(Transform transformer)
    {
        for (int i = 0; i < parabolaBases.Count; i++)
        {
            if (parabolaBases[i].transformer.Equals(transformer))
                parabolaBases.Remove(parabolaBases[i]);
        }
    }

    public void ClearBases()
    {
        parabolaBases.Clear();
    }
}