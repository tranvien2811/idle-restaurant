﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonEffector : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private Vector3 scale;

    public float scaleBonus = 0.25f;

    private void OnEnable()
    {
        scale = transform.localScale;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        transform.localScale = new Vector3(
            scale.x + scaleBonus,
            scale.y + scaleBonus,
            scale.z + scaleBonus);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        transform.localScale = scale;
    }
}