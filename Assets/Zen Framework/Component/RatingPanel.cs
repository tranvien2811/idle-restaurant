﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class RatingData
{
    public int rated;
}

public class RatingPanel : MonoBehaviour
{
    private RatingData data;

    [SerializeField] private Transform grid;
    [SerializeField] private string url;
    [SerializeField] private int goal;

    private void OnEnable()
    {
        OnInit();
    }

    private void OnInit()
    {
        Load();
        int number = data.rated;
        if (grid == null)
            return;
        for (int i = 0; i < grid.childCount; i++)
        {
            if (!grid.GetChild(i).GetComponent<Button>())
                continue;
            Button btn = grid.GetChild(i).GetComponent<Button>();
            if (i + 1 > number)
                btn.image.color = Color.gray;
            else
            {
                btn.image.color = Color.white;
                btn.interactable = false;
            }
        }
    }

    private void Load()
    {
        data = new RatingData()
        {
            rated = 0
        };

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "rating_data.xml"))
            data = XmlPath.GetByPlatform<RatingData>("rating_data");
    }

    private void Save()
    {
//        XmlPath.SetByPlatform("rating_data", data);
    }

    public void OnRating(int rated)
    {
        if (data == null)
            return;
        int number = data.rated;
        if (rated + 1 <= number)
            return;
        for (int i = 0; i < grid.childCount; i++)
        {
            if (!grid.GetChild(i).GetComponent<Button>())
                continue;
            Button btn = grid.GetChild(i).GetComponent<Button>();
            if (i > rated)
                btn.image.color = Color.gray;
            else
            {
                btn.image.color = Color.white;
                btn.interactable = false;
            }
        }

        if (rated + 1 >= goal)
            Application.OpenURL(url);
        data.rated = rated + 1;
    }
}