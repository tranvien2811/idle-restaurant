using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SnapScroll : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    private enum Scroll
    {
        Horizontal,
        Vertical
    }

    private enum State
    {
        None,
        Moving,
        Snapping
    }

    private State state;
    private float moveSpeed;
    private int snap;
    private Vector2 clickPoint;
    private Vector2 dragPoint;
    private Vector2 direction;

    [SerializeField] private Scroll kind;
    [SerializeField] private float speed = 1.0f;
    [SerializeField] private float margin = 150.0f;
    [SerializeField] private float scale = 0.5f;
    [SerializeField] private int prefabs = 10;
    [SerializeField] private Canvas canvas;
    [SerializeField] private Transform grid;
    [SerializeField] private GameObject prefab;

    private void OnEnable()
    {
        Init();
    }

    private void Update()
    {
        Move();
    }

    private void Init()
    {
        if (grid == null ||
            prefab == null ||
            grid.childCount >= prefabs)
            return;
        for (int i = 0; i < prefabs; i++)
        {
            GameObject obj = Instantiate(prefab, grid);
            obj.SetActive(true);
        }
    }

    private void Sort()
    {
        if (grid == null ||
            grid.childCount <= 0)
            return;
        float marginScale = 1.0f;
        if (canvas != null)
        {
            Vector2 resolution = canvas.GetComponent<CanvasScaler>().referenceResolution;
            switch (kind)
            {
                case Scroll.Horizontal:
                    marginScale = Screen.width / resolution.x;
                    break;
                case Scroll.Vertical:
                    marginScale = Screen.height / resolution.y;
                    break;
            }
        }

        for (int i = 0; i < grid.childCount; i++)
        {
            if (i.Equals(0))
                continue;
            switch (kind)
            {
                case Scroll.Horizontal:
                    grid.GetChild(i).position = new Vector2(
                        grid.GetChild(i - 1).position.x + margin * marginScale,
                        grid.GetChild(0).position.y);
                    break;
                case Scroll.Vertical:
                    grid.GetChild(i).position = new Vector2(
                        grid.GetChild(0).position.x,
                        grid.GetChild(i - 1).position.y - margin * marginScale);
                    break;
            }
        }

        List<float> distances = new List<float>();
        for (int i = 0; i < grid.childCount; i++)
        {
            float distance = Vector2.Distance(
                grid.GetChild(i).position,
                grid.position);
            distances.Add(distance);
        }

        if (distances.Count > 0)
        {
            for (int i = 0; i < distances.Count; i++)
            {
                if (!distances[i].Equals(distances.Min()))
                    continue;
                snap = i;
            }
        }

        float originScale = 0;
        switch (kind)
        {
            case Scroll.Horizontal:
                originScale = prefab.transform.localScale.x;
                break;
            case Scroll.Vertical:
                originScale = prefab.transform.localScale.y;
                break;
        }

        for (int i = 0; i < grid.childCount; i++)
        {
            if (i.Equals(snap))
            {
                grid.GetChild(i).localScale = new Vector2(
                    originScale + scale,
                    originScale + scale);
            }
            else
                grid.GetChild(i).localScale = new Vector2(originScale, originScale);
        }
    }

    private void SetState(State param, float value)
    {
        state = param;
        moveSpeed = value;
    }

    private bool IsClamp()
    {
        bool isResult = false;
        if (grid != null &&
            grid.childCount > 0)
        {
            switch (kind)
            {
                case Scroll.Horizontal:
                    if (grid.GetChild(0).position.x >= grid.position.x ||
                        grid.GetChild(grid.childCount - 1).position.x <= grid.position.x)
                    {
                        isResult = true;
                    }

                    break;
                case Scroll.Vertical:
                    if (grid.GetChild(0).position.y <= grid.position.y ||
                        grid.GetChild(grid.childCount - 1).position.y >= grid.position.y)
                    {
                        isResult = true;
                    }

                    break;
            }
        }

        return isResult;
    }

    private void Move()
    {
        Sort();
        if (grid == null ||
            grid.childCount <= 0 ||
            direction.Equals(Vector2.zero))
            return;
        switch (kind)
        {
            case Scroll.Horizontal:
                direction = new Vector2(direction.x, 0);
                break;
            case Scroll.Vertical:
                direction = new Vector2(0, direction.y);
                break;
        }

        for (int i = 0; i < grid.childCount; i++)
        {
            grid.GetChild(i).Translate(direction * moveSpeed * Time.deltaTime);
        }

        switch (state)
        {
            case State.Moving:

                if (moveSpeed > 0)
                    moveSpeed -= Time.deltaTime;
                else
                {
                    clickPoint = Vector2.zero;
                    direction = grid.position - grid.GetChild(snap).position;
                    if (IsClamp())
                        direction = new Vector2(direction.x * 2.0f, direction.y * 2.0f);
                    SetState(State.Snapping, speed / 2);
                }

                break;
            case State.Snapping:
                Vector2 face = grid.position - grid.GetChild(snap).position;
                float distance = Vector2.Distance(grid.GetChild(snap).position, grid.position);
                if (direction.x < 0 && face.x < 0 && distance > 0.1f ||
                    direction.x > 0 && face.x > 0 && distance > 0.1f)
                    return;
                SetState(State.None, 0);
                break;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (clickPoint.Equals(Vector2.zero))
        {
            clickPoint = eventData.position;
        }
        else
        {
            dragPoint = eventData.position;
            direction = dragPoint - clickPoint;
            SetState(State.Moving, speed);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (clickPoint.Equals(Vector2.zero))
            return;
        direction = eventData.position - clickPoint;
        clickPoint = Vector2.zero;
        SetState(State.Moving, speed);
    }

    public void Clear()
    {
        for (int i = 0; i < grid.childCount; i++)
        {
            Destroy(grid.GetChild(i));
        }
    }

    public void ReInit()
    {
        Clear();
        Init();
    }

    public void SetSnap(int value)
    {
        snap = value;
    }

    public int GetSnap()
    {
        int number = snap;
        return number;
    }
}