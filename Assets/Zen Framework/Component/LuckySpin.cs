﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class LuckySpin : MonoBehaviour
{
    private float currenSpeed;
    private float targetAngle;
    private float currentAngle;
    private int result;
    private bool isSpin;

    [SerializeField] private Transform slotContainer;
    [SerializeField] private float spinSpeed;
    [SerializeField] private float spinAngle;
    [SerializeField] private float[] slotRates;

    private void Update()
    {
        OnRotation();
    }

    private int GetResultSlot()
    {
        List<float> rates = new List<float>();
        int number = 0;

        for (int i = 0; i < slotRates.Length; i++)
        {
            float rate = Random.Range(0, slotRates[i]);
            if (!rates.Contains(rate))
                rates.Add(rate);
        }

        if (rates.Count > 0)
        {
            for (int i = 0; i < rates.Count; i++)
            {
                if (rates[i] < rates.Max())
                    continue;
                number = i;
            }
        }

        return number;
    }

    private float GetAngle(int newResult)
    {
        int step = newResult;
        float angleResult = (step * spinAngle) + (slotRates.Length * 360);
        return angleResult;
    }

    private void OnRotation()
    {
        if (!isSpin || slotContainer == null || Time.timeScale <= 0)
            return;
        if (currentAngle < targetAngle)
        {
            float angelPercent = (currentAngle * 100) / targetAngle;
            float inversePercent = 100.0f - angelPercent;
            currenSpeed = (spinSpeed * inversePercent) / 100;
            if (currenSpeed <= 1.0f)
                currenSpeed = 1.0f;
            currentAngle += currenSpeed;
            slotContainer.rotation = Quaternion.Euler(0, 0, currentAngle);
        }
        else
        {
            targetAngle = slotContainer.transform.rotation.z;
            currentAngle = targetAngle;
            isSpin = false;
        }
    }

    public void OnSpin()
    {
        isSpin = true;
        int newResult = GetResultSlot();
        targetAngle += GetAngle(newResult);
        result = newResult;
        currenSpeed = spinSpeed;
    }

    public int GetResult()
    {
        return result;
    }
}