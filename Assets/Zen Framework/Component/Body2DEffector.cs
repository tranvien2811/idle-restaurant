using UnityEngine;

[System.Serializable]
public enum Body
{
    Dynamic,
    Kinematic,
    Static
}

[System.Serializable]
public enum Reflection
{
    None,
    TopToBottom,
    LeftToRight,
    RightToLeft,
    BottomToTop,
}

[System.Serializable]
public enum Vector
{
    All,
    x,
    y,
    z
}

public abstract class Body2DEffector : MonoBehaviour
{
    public static void SetType(Rigidbody2D body2D, Body kind)
    {
        if (body2D == null)
            return;
        switch (kind)
        {
            case Body.Dynamic:
                body2D.bodyType = RigidbodyType2D.Dynamic;
                break;
            case Body.Kinematic:
                body2D.bodyType = RigidbodyType2D.Kinematic;
                break;
            case Body.Static:
                body2D.bodyType = RigidbodyType2D.Static;
                break;
        }
    }

    public static void SetGravity(Rigidbody2D body2D, float gravity)
    {
        if (body2D != null)
            body2D.gravityScale = gravity;
    }

    public static void Reflect(Rigidbody2D body2D, Reflection reflection, float power)
    {
        if (body2D == null || body2D.bodyType == RigidbodyType2D.Static)
            return;
        switch (reflection)
        {
            case Reflection.TopToBottom:
                body2D.velocity = new Vector2(body2D.velocity.x, -power);
                break;
            case Reflection.LeftToRight:
                body2D.velocity = new Vector2(power, body2D.velocity.y);
                break;
            case Reflection.RightToLeft:
                body2D.velocity = new Vector2(-power, body2D.velocity.y);
                break;
            case Reflection.BottomToTop:
                body2D.velocity = new Vector2(body2D.velocity.x, power);
                break;
        }
    }

    public static void Constraints(Rigidbody2D body2D, Vector vector)
    {
        if (body2D == null)
            return;
        switch (vector)
        {
            case Vector.All:
                body2D.velocity = new Vector2(0, 0);
                body2D.transform.rotation = Quaternion.identity;
                break;
            case Vector.x:
                body2D.velocity = new Vector2(0, body2D.velocity.y);
                break;
            case Vector.y:
                body2D.velocity = new Vector2(body2D.velocity.x, 0);
                break;
            case Vector.z:
                body2D.transform.rotation = Quaternion.identity;
                break;
        }
    }

    public static void Velocity(Rigidbody2D body2D, Vector2 direction, float power)
    {
        if (body2D != null && body2D.bodyType != RigidbodyType2D.Static)
            body2D.velocity = direction * power;
    }

    public static void Velocity(Rigidbody2D body2D, Vector vector, float power)
    {
        if (body2D == null || body2D.bodyType == RigidbodyType2D.Static)
            return;
        switch (vector)
        {
            case Vector.All:
                body2D.velocity = new Vector2(power, power);
                break;
            case Vector.x:
                body2D.velocity = new Vector2(power, body2D.velocity.y);
                break;
            case Vector.y:
                body2D.velocity = new Vector2(body2D.velocity.x, power);
                break;
        }
    }

    public static void Velocity(Rigidbody2D body2D, float powerX, float powerY)
    {
        if (body2D != null && body2D.bodyType != RigidbodyType2D.Static)
            body2D.velocity = new Vector2(powerX, powerY);
    }

    public static void ClampVelocity(Rigidbody2D body2D, float max)
    {
        if (body2D == null || body2D.bodyType == RigidbodyType2D.Static)
            return;
        if (body2D.velocity.x >= max)
            body2D.velocity = new Vector2(max, body2D.velocity.y);
        else if (body2D.velocity.x <= -max)
            body2D.velocity = new Vector2(-max, body2D.velocity.y);

        if (body2D.velocity.y >= max)
            body2D.velocity = new Vector2(body2D.velocity.x, max);
        else if (body2D.velocity.y <= -max)
            body2D.velocity = new Vector2(body2D.velocity.x, -max);
    }

    public static void Destroy(Rigidbody2D body2D)
    {
        if (body2D != null)
            Object.Destroy(body2D);
    }

    public static Rigidbody2D GetBody(GameObject obj)
    {
        Rigidbody2D body = null;
        if (obj != null &&
            obj.GetComponent<Rigidbody2D>())
            body = obj.GetComponent<Rigidbody2D>();
        return body;
    }
}