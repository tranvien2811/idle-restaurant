﻿using System;

[Serializable]
public struct RestaurantProperties
{
    public double loadingSpeed;
    public double loadPerWaiter;
    public double transportation;

    public float walkingSpeed;

    public int waiter;
}

[Serializable]
public struct KitchenProperties
{
    public double workingSpeed;
    public double totalExtraction;
    public double transporterCapacity;

    public float walkingSpeed;

    public int transporter;
}

[Serializable]
public struct ElevatorProperties
{
    public double load;
    public double loadingSpeed;
    public double transportation;

    public float movementSpeed;
}

public struct GameStructs
{
}