using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Boost
{
    public int duration;
    public int effective;
    public int remaining;
}

[Serializable]
public class BoostData
{
    public int boostRemaining;

    public List<Boost> boosts;

    public BoostData()
    {
        this.boosts = new List<Boost>();
    }
}

public class BoostController : MonoBehaviour
{
    [HideInInspector] public int upgradeCostReduced;
    [HideInInspector] public float walkingSpeedBoost;
    [HideInInspector] public float cookingSpeedBoost;
    [HideInInspector] public float loadingSpeedBoost;
    [HideInInspector] public float loadExpansionBoost;
    [HideInInspector] public float movementSpeedBoost;

    public void Refresh()
    {
        this.walkingSpeedBoost = 1f; //moving speed
        this.cookingSpeedBoost = 1f; //working speed
        this.loadingSpeedBoost = 1f; //evelator speed
        this.loadExpansionBoost = 1f; //evelator contain
        this.movementSpeedBoost = 1f; //manager speed bonus
        this.upgradeCostReduced = 0; //manager upgrade cost
    }

    public void SetWalkingSpeedBoost(float value)
    {
        walkingSpeedBoost = value;
    }
}