﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Includer : MonoBehaviour
{
    private WebPath mWebPath;

    #region properties

    private WebPath WebPath
    {
        get
        {
            if (mWebPath == null && GetComponent<WebPath>())
                mWebPath = GetComponent<WebPath>();
            return mWebPath;
        }
    }

    #endregion

    private void OnEnable()
    {
        DontDestroyOnLoad(this);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(WebPath.OnGetdata("https://mocki.io/v1/f9754e9f-01fa-4c7a-880e-451318c65552"));
            StartCoroutine(OnBuild());
        }
    }

    private IEnumerator OnBuild()
    {
        yield return new WaitForSeconds(0.5f);
        OnBuilding();
    }

    private void OnBuilding()
    {
        JSONNode jsonNode = JSON.Parse(System.Text.Encoding.UTF8.GetString(WebPath.GetDownloadedDatas()));
        if (jsonNode == null)
            Debug.Log("json is null!");
        else
        {
            Debug.Log("ID: " + jsonNode["ID"] + '\n');
            Debug.Log("name: " + jsonNode["name"] + '\n');
            Debug.Log("str: " + jsonNode["str"] + '\n');
            Debug.Log("speed: " + jsonNode["speed"] + '\n');
            Debug.Log("active: " + jsonNode["active"] + '\n');
        }
    }
}