using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Database
{
    public double cash;

    public int diamond;
    public int targetRestaurant;

    public List<Item> item;
    public List<string> nonConsume;

    public FreeCoinData freeCashData;

    public SoundSetting soundSetting;

    public List<Restaurant> restaurant;
    public List<int> tutorialCompleted;

    public Database(Configuration config)
    {
        this.item = new List<Item>();
        this.nonConsume = new List<string>();
        this.restaurant = new List<Restaurant>();
        this.restaurant.Add(new Restaurant(config));
        this.tutorialCompleted = new List<int>();
        this.soundSetting = new SoundSetting();
        this.freeCashData = new FreeCoinData();
        this.cash = config.general.startCash;
        this.diamond = config.general.startDiamond;
    }
}

public class DataManager : Singleton<DataManager>
{
    [NonSerialized] public Database database;

    [SerializeField] private Configuration configuration;

    private void Awake()
    {
        this.LoadDatabase();
    }

    private void LoadDatabase()
    {
        string text = string.Empty;
        if (!PlayerPrefs.HasKey(this.configuration.general.dataName))
        {
            text = JsonUtility.ToJson(new Database(this.configuration));
            PlayerPrefs.SetString(this.configuration.general.dataName, text);
        }

        text = PlayerPrefs.GetString(this.configuration.general.dataName);
        this.database = JsonUtility.FromJson<Database>(text);
    }

    private void SaveDatabase()
    {
        this.database.restaurant[this.database.targetRestaurant].dateTime = DateTime.Now.ToString();
        string value = JsonUtility.ToJson(this.database);
        PlayerPrefs.SetString(this.configuration.general.dataName, value);
    }

    private void OnApplicationQuit()
    {
        this.SaveDatabase();
    }

    private void OnApplicationPause(bool paused)
    {
        if (paused)
            this.SaveDatabase();
    }
}