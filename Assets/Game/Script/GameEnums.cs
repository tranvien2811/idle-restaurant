﻿public enum ManagerState
{
    Ready,
    Boosting,
    Cooldown
}

public enum ManagerSkill
{
    UpgradeCost,
    WalkingSpeed,
    CookingSpeed,
    LoadingSpeed,
    MovementSpeed,
    LoadExpansion
}

public enum Experience
{
    Junior,
    Senior,
    Expert,
    Master
}

public enum Location
{
    Kitchen,
    Elevator,
    Restaurant
}

public enum BarrierState
{
    Locked,
    Process,
    Unlocked
}

public enum GameEnums
{
}